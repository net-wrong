# :stopdoc:
require 'net/protocol'
require 'io/wait'
require 'kgio'
require 'terrible_timeout'

class Net::BufferedIO
  alias_method :old_rbuf_fill, :rbuf_fill
  undef_method :rbuf_fill

  # avoids GC thrashing, Kgio.tryread won't release the GVL
  # (unlike IO#read_nonblock), so there's no concurrency issue
  # with locked strings
  WRONG_BUF = "".encode("binary")

  def rbuf_fill
    case @io
    when IO
      case rv = Kgio.tryread(@io, 16384, WRONG_BUF)
      when String
        return @rbuf << rv
      when :wait_readable
        @io.wait(@read_timeout) or raise Timeout::Error
      when nil
        raise EOFError, "end of file reached"
      else
        raise "BUG: Kgio.tryread returned: #{rv.inspect}"
      end while true
    else # SSL sockets, deal with this later
      old_rbuf_fill
    end
  end
end

module Net::Wrong
  VERSION = '1.0.1'

  # I would do something with IO.select + Kgio::Start, but timing out
  # DNS requests that return multiple addresses is complicated, so
  # we'll just be lazy and use the terrible version:
  def timeout(maxtime, ignored = nil)
    TerribleTimeout.run(maxtime) { yield }
  end
end
