require './test/helper'
require 'net/smtp'
require 'net/wrong/smtp' unless ENV["TEST_FAIL"]

class TestNetWrongSMTP < Test::Unit::TestCase
  include TestNetWrong

  def setup
    srv_setup
    @thr = Thread.new do
      client = @srv.accept
      client.write("220 death ESMTP Postfix (Debian/GNU)\r\n")
      client.write("EHLO localhost\r\n")
      client.write("250-death\r\n")
      client.write("250 DSN\r\n")
      client.gets
      client.write("221 2.0.0 Bye\r\n")
      IO.select([client])
      client.close
    end
  end

  def test_esmtp
    check_eagain do
      Net::SMTP.start(@host, @port) { |smtp| assert_kind_of(Net::SMTP, smtp) }
    end
  end
end
