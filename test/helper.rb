require 'test/unit'
require 'socket'

module TestNetWrong
  def srv_setup
    @host = "127.0.0.1"
    @srv = TCPServer.new(@host, 0)
    @port = @srv.addr[1]
  end

  def check_eagain
    GC.start
    GC.disable
    prev = ObjectSpace.each_object(Errno::EAGAIN) { |_| }
    yield
    assert_equal prev, ObjectSpace.each_object(Errno::EAGAIN) { |_| }
  end
end
