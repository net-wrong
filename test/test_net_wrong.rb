require './test/helper'
require 'stringio'

class TestNetWrongBasics < Test::Unit::TestCase
  def test_require_telnet
    assert_nothing_raised { require "net/wrong/telnet" }
  end

  def test_require_pop
    assert_nothing_raised { require "net/wrong/pop" }
  end

  def test_buffered_io_nonio_fallback
    require 'net/wrong'
    sio = StringIO.new("foo")
    b = Net::BufferedIO.new(sio)
    assert_equal "", b.instance_variable_get(:@rbuf)
    assert_nothing_raised { b.method(:old_rbuf_fill) }
    assert_equal "foo", b.rbuf_fill
    assert_equal "foo", b.instance_variable_get(:@rbuf)
  end
end unless ENV["TEST_FAIL"]
