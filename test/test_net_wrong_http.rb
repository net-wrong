require './test/helper'
require 'net/http'
require 'net/wrong/http' unless ENV["TEST_FAIL"]
require 'webrick'

class TestNetWrongHTTP < Test::Unit::TestCase
  include TestNetWrong

  class Testlet < WEBrick::HTTPServlet::AbstractServlet
    def service(req, res)
      res.status = 200
      res["Content-Type"] = "text/plain"
      res["Content-Length"] = "3"
      res.body << "HI\n"
    end
  end

  def setup
    srv_setup
    @webrick = fork do
      server = WEBrick::HTTPServer.new(:DoNotListen => true)
      server.listeners << @srv
      server.mount("/", Testlet)
      server.start
    end
    @srv.close
  end

  def teardown
    GC.enable
    Process.kill(:KILL, @webrick)
    Process.waitpid2(@webrick)
  end

  def test_keepalive
    req = Net::HTTP::Get.new("/")
    check_eagain do
      Net::HTTP.start(@host, @port) do |http|
        http.read_timeout = 5
        http.open_timeout = 5
        assert_equal "HI\n", http.request(req).body
        assert_equal "HI\n", http.request(req).body
        assert_equal "HI\n", http.request(req).body
      end
    end
  end
end
